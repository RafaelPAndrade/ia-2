# -*- coding: utf-8 -*-
"""
Grupo 014 Alameda
88080 Miguel Francisco
86503 Rafael Andrade
"""



class Node():
    def __init__(self, prob, parents = []):
        self.prob = prob
        self.parents = parents

    def computeProb(self, evid):
        asked_probability = self.prob

        for parent in self.parents:
            asked_probability = asked_probability[evid[parent]]

        return [1 - asked_probability, asked_probability]


class BN():
    def __init__(self, gra, prob):
        self.gra = gra
        self.prob = prob

    def computeJointProb(self, evid):
        final = 1
        for p in range(0,len(self.prob)):
            final = final * self.prob[p].computeProb(evid)[evid[p]]
        return final


    def computePostProb(self, evid):
        def iterator(evid):
            evid = list(evid)
            variable_items = []
            for i in range(len(evid)):
                if evid[i] == []:
                    variable_items.append(i)

            # All combinations: 2**number of variables
            for combo in range(2**len(variable_items)):
                for i in range(len(variable_items)):
                    evid[variable_items[i]] = ( combo >> i ) % 2
                yield evid

        numerator = 0
        denominator = 0
        unknown_node = evid.index(-1)


        for possibility in iterator(evid):
            possibility[unknown_node] = 1
            numerator += self.computeJointProb(possibility)
            possibility[unknown_node] = 0
            denominator += self.computeJointProb(possibility)

        denominator += numerator

        return numerator/denominator
