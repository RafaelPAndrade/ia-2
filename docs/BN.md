---
title: Relatório do projeto 2 de IA - Redes Bayesianas
subtitle: Grupo A014
author:
- Miguel Francisco, n. 88080
- Rafael Andrade, n. 86503
papersize: A4
fontsize: 14pt
geometry: "hmargin=2.2cm,vmargin=1cm"
---

# Rede Bayesiana

Começando pela função `computeProb` da classe `Node`, os resultados pedidos representam a probabilidade de um nó tendo em conta a informação e probabilidade dos seus pais, ou seja, dos nós que o influenciam diretamente. No seu cálculo, são percorridos todos os seus pais, se existirem, e utiliza-se a probabilidade da tabela de verdade correspondente à evidência. Logo, no pior caso, um nó é filho de todos os outros nós de uma rede, sendo a sua complexidade $O(N)$, com $N$ o número de nós da rede. Esta função não apresenta limitações dado que aceita qualquer número de pais.
Os resultados obtidos foram os esperados, mas diferentes dos do enunciado, dado que nestes últimos surgem casas decimais a mais, representando o valor decimal verdadeiro do valor guardado em binário, consequência da representação de valores decimais com formato _Floating Point_.

Relativamente à função `computeJointProb`, da classe `BN`, os resultados obtidos representam a probabilidade conjunta de diferentes nós de uma rede Bayesiana, tendo em conta o valor de verdade de cada nó, representado através de uma lista de evidências. O cálculo dessa probabilidade tem por base a seguinte expressão, usando como exemplo a rede do enunciado:

$$ P(B \cap E \cap A \cap J \cap \neg M) = P(\neg M | A ) \times P(J | A) \times P(A | B \cap E) \times P(E) \times P(B) $$

Onde o cálculo de cada probabilidade é feita através da função `computeProb` com a evidência fornecida. Como aqui não há nós com valor de verdade desconhecido, esta probabilidade é sempre calculada ao percorrer todos os nós da rede, pelo que esta depende do tamanho e estrutura da rede Bayesiana e da complexidade da função `computeProb`.
Veja-se o exemplo em que um nó da rede, `prob[i]`, é filho de todos os nós anteriormente criados, `prob[:i-1]`. Aqui a complexidade da função é $O(N^2)$, que é o pior caso.
O resultado pedido no ficheiro de testes era a soma de todas as probabilidades conjuntas de uma rede, que é obviamente 1, o qual foi obtido pela nossa implementação.

Os resultados pedidos da função `computePostProb` da classe `BN` representam a probabilidade _a posteriori_ de um nó, tendo em conta o valor de verdade de todos os nós da rede, representados pelas evidências. Foi considerada para esta função, a seguinte expressão de probabilidade condicionada, considerando a rede Bayesiana do enunciado:

$$ P(A | B \cap E) =  \frac {P(A \cap B \cap E) } {P( A \cap B \cap E) + P(\neg A \cap B \cap E)} $$
Onde, por exemplo,
$$ P(A \cap B \cap E) = \sum_{J, M \in \{T, F\}} P(A \cap B \cap E \cap J \cap M) $$

Esta última é calculada pela função auxiliar `iterator` que, através do número de variáveis desconhecidas da evidência fornecida ($D$), gera as $2^{D}$ diferentes evidências possíveis (com 0 ou 1); para colocar todos os valores necessários a 0 ou 1 são necessários $O(D)$ passos. Assim a complexidade desta função é $O(2^{D} \times D)$. Como `computePostProb` consiste em aplicar a função `computeJointProb` (que tem complexidade $O(N^2)$) a cada uma das $O(2^{D})$ evidências, a complexidade total é $O(2^D\times(D + N^2))$. Tendo em conta que $D < N \implies D = O(N)$, a complexidade total da função `computePostProb` é $O(2^N\times N^2)$.

É de notar que uma das limitações desta implementação é a não simplificação no cálculo de probabilidades de nós independentes, tratando de forma homogénea todos os nós.
Além disto, esta poderia ser implementada de forma recursiva ou em árvore. Neste caso, existiriam $2^D$ nós terminais, todos à profundidade igual ao número de nós na rede ($N$); a cada nível corresponde uma variável, e nos níveis que são variáveis desconhecidas existiria uma bifurcação; a vantagem desta possível implementação está na reutilização de cálculos já feitos, e a complexidade final seria $O(2^{N})$.

