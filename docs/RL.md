---
title: Relatório do projeto 2 de IA - Aprendizagem por Reforço
subtitle: Grupo A014
documentclass: article
author:
- Miguel Francisco, n. 88080
- Rafael Andrade, n. 86503
papersize: A4
fontsize: 14pt
geometry: "hmargin=2.2cm,vmargin=1cm"
header-includes:
  - \usepackage{multicol}
  - \setlength{\columnseprule}{1pt}
  - \usepackage{graphicx}
  - \graphicspath{ {./docs/} }
  - \usepackage{wrapfig}
  - \usepackage{float}
---

## Ambientes e Movimentação de agente


\begin{figure}[H]
\centering
\includegraphics[width=10cm]{worlds.png}
\caption{Ambiente onde o agente se move}
\end{figure}


O ambiente do exercício 1 pode ser visto como uma espécie de corredor em
que os estados estão ordenados. Executar a ação 1 faz com que se transite
do estado $s$ para o estado $s-1$ (e permanecemos no mesmo estado se estivermos
na ponta e "batermos na parede"), e executar a ação 0 transitamos para
o estado $s+1$, excepto se estivermos no estado 5; nesse caso, existe uma
probabilidade de $0.1$ de ficarmos no estado 5 (e ainda podemos
"bater na parede").
Além disto, quando o agente chega ao estado 0 ou 6 e depois tenta realizar
uma qualquer ação, é colocado no estado inicial da sua trajetória,
que neste exercício foi o estado 3.

O ambiente no exercício 2 pode ser visto como um anel, como o representado
na figura. Neste ambiente também existe a dinâmica de tentar realizar um
movimento contra a parede resulta no agente permanecer no mesmo sítio.
Para além disto, se o agente estiver no estado 7 e realizar a ação 0,
em vez de continuar no estado 7, irá parar ao estado 1. No entanto, para ir
do estado 1 para o 7, não utiliza a ação inversa, que seria a 3, mas sim a
ação 2.

## Funções de Recompensa

\begin{multicols}{2}
\subsubsection{Exercício 1}
$$ R(s, a) = \begin{cases}1 & s \in {0,6} \\0  & c.c. \end{cases} $$

\subsubsection{Exercício 2}
$$ R(s, a) = \begin{cases}0 & s = 7       \\-1 & c.c. \end{cases} $$
\end{multicols}


## Política Óptima

Para o exercício 1, a política óptima consistem em chegar a um dos
estados com recompensa (estado 0 ou 6) e permanecer lá,
indo contra as paredes do mundo. Portanto, a partir da posição
$s \in \{0,1,2\}$
$\pi^{*}(s) = 1$; para $s = 6$, $\pi^{*}(s) = 0$.
Nas posições $s \in {3,4,5}$,
existe uma subtileza:  tentar chegar ao estado 6 implica arriscar tomar
a ação 0 no estado 5 e portanto ficar no mesmo estado. Uma vez que neste
exercício as recompensas são recompensas descontadas
($\gamma  = 0.9 < 1$), quanto maior
for o comprimento do trajeto, menor a utilidade. Isto é visível no estado 3:
$\pi^{*}(3) = 1$, que é ir para o estado 0, apesar de em teoria ser indiferente
ir para o estado 0 ou 6, uma vez que são equidistantes. Por outro lado,
$\pi^{*}(4) = \pi^{*}(5) = 0$, porque apesar da possibilidade de permanecer no
mesmo estado 5 durante o seu trajeto,
isso tem uma probabilidade reduzida de acontecer, e estes estados estão mais
próximos do estado 6 do que do 0.


A política óptima do exercício 2 é chegar ao estado 7 e permanecer lá,
pois esse é o único estado cuja recompensa não é negativa. Para ficar nesse
estado, basta realizar a ação 1 (ir contra a parede direita); logo, para cada
estado, $\pi^{*}$ consiste em ir para o estado cuja _Manhattan distance_ a
$s=7$ é menor, tendo atenção que pode valer a pena ir até $s=1$ e aí aplicar
a ação 2.


## Movimentação do Agente

Quando estamos na fase de _exploitation_, o agente escolhe em cada estado a
ação cujo valor na matriz $Q$ é o maior.
Na fase de _exploration_, aplicada no exercício 1,
decidimos escolher fazer a ação que
foi tomada menos vezes em cada estado.


## Complexidade Computacional e outras alternativas

O primeiro método implementado, o **traces2Q**, consiste apenas na aplicação
sucessiva da fórmula de QLearning a cada transição de uma trajetória
(uma transição de trajetória é um tuplo
_<estado inicial, ação, estado final, recompensa> = <s, a, s', r>_),
até que a diferença entre aplicações seja
negligível (menor que $1\times10^{-7}$).

$$ Q(s,a) = Q(s,a) + \alpha(r + \gamma\max_{a'} Q(s',a') - Q(s,a)) $$

Porque é necessário encontrar a ação $a'$ que maximiza $Q(s',a')$,
cada aplicação da fórmula numa dada transição
tem um tempo $O(nA)$,
em que $nA$ é o número de ações (é preciso considerar $nA$ ações para saber
qual é que maximiza $Q$ no estado final da transição). Como esta fórmula é
aplicada $T$ vezes se o tamanho da trajetória for $T$,
então cada aplicação da fórmula
à trajetória completa é $O(T\times nA)$. Se for preciso aplicar $k$ vezes a
fórmula à trajetória até que os valores de $Q$ convirjam, então a complexidade
temporal será $O(k\times T\times nA)$. O valor $k$ depende dos valores de
$\alpha$, $\gamma$ e do ambiente (e se este for estocástico, o valor $k$ será
praticamente aleatório).

O segundo método implementado foi **policy**, que dita qual a ação que o agente
toma, dependendo do estado atual, do tipo de política queremos considerar
(*exploration* ou *exploitation*), e da matriz $Q$ calculada anteriormente.
Se for pedida uma ação com uma política de *exploitation*, será retornada a
ação que a matriz $Q$ indica ter a maior utilidade esperada.
Se for pedida uma ação com uma política de *exploration*, será retornada a ação
que foi efetuada menos vezes naquela posição[^temosN].
Em qualquer um destes casos, a complexidade de escolher uma ação é $O(nA)$
(em que $nA$ é o número de ações possíveis), pois em qualquer uma delas temos
de determinar um máximo ou um mínimo entre $nA$.

[^temosN]: A nossa classe finiteMDP guarda uma matriz que mantém uma contagem
de quantas vezes uma dada ação foi tomada num dado estado.


Se quiséssemos reduzir a complexidade de *policy*,
poderíamos utilizar uma estratégia
menos custosa quando estamos em *exploration*; por exemplo, fazer uma ação
aleatória. No entanto, com esta estratégia poderia ser necessário um trajeto
maior durante a aprendizagem para depois termos uma trajetória ótima.
Além disso, nos ambientes testados, verificamos que $nA$ é constante e
relativamente pequeno, pelo que não há grandes ganhos neste *tradeoff*.


## Resultados práticos

No exercício 1,
para determinar o melhor valor de $\alpha$ e de tamanho do trajeto de treino
($T$), testámos todos os valores de $\alpha$ entre 0 e 1 em incrementos de
0.05 e valores de $T$ entre 50 e 2000 em incrementos de 25, e contámos quantas
vezes em 10 é que trajeto em modo *exploitation*, realizado após a aplicação
do método **traces2Q** aplicado ao dito trajeto de treino,
foi óptimo.
Abaixo mostramos o gráfico resultante.

\begin{wrapfigure}{l}{0.75\textwidth}
\includegraphics[width=0.75\textwidth]{alpha_t_cropped}
\caption{Número de vezes em 10 que a trajetória encontrada pelo agente
foi a óptima. Preto: 0/10; Branco: 10/10}
\end{wrapfigure}

Daqui podemos concluir que um valor de $\alpha$ relativamente baixo,
por exemplo $\alpha = 0.20$, é melhor que valores de $\alpha$ muito grandes,
uma vez que encontram a trajetória óptima com um menor $T$;
com $\alpha = 0.2$, a partir de $T = 800$, a trajetória encontrada é a óptima,
e permanece assim pelo menos até $T = 2000$. Note-se que 10 testes para cada
par $<\alpha, T>$ é insuficiente,
mas testes mais extensivos tomariam demasiado tempo.
Além disso, como esperado,
verifica-se que com $T$ crescente a performance do agente melhora.
