# Main Makefile
# IA 2018/19
# Rafael Andrade, Miguel Francisco
# P2

SHELL         := /bin/sh
RELEASE_NAME  := A014


SRC_FILES   := RL.py BN.py
TST_DIR     := tests
TST_ZIP     :=

TST_COMM    := ./test.sh

ZIP         := zip
UNZIP       := unzip -qo
ZIP_FLAGS   := -ur --quiet


REPORTS     := docs/relatorioA014.pdf

.DEFAULT_GOAL := report


.PHONY : zip test report analysis
zip :
	$(ZIP) $(ZIP_FLAGS) $(RELEASE_NAME).zip $(SRC_FILES) ||:
	cd docs/ && $(ZIP) $(ZIP_FLAGS) ../$(RELEASE_NAME).zip $(REPORTS:docs/%=%) ||:

test :
	cd $(TST_DIR) && $(UNZIP) $(TST_ZIP).zip
	cd $(TST_DIR) && $(TST_COMM)

report : $(REPORTS)

r: docs/BN.pdf docs/RL.pdf

%.pdf: %.md
	pandoc -f markdown -t latex $< -o $@

%.md:
	$(error "Missing Markdown file: $@")
